Informixdb dialect for SQLAlchemy.

This dialect provides support for the informixdb project:

	http://sourceforge.net/p/informixdb/

Note that this dialect is currently unmaintained.   For a production-level
informix dialect, see the IBM DB project at:

	http://code.google.com/p/ibm-db/


