from sqlalchemy.dialects import registry

registry.register("informixdb", "sqlalchemy_informixdb.informixdb", "InformixDialect_informixdb")

from sqlalchemy.testing import runner

runner.main()
